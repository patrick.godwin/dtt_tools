# dtt_tools

Stores tools related to DTT.

There is a single script currently in this package, `dtt2tfplot`, which creates transfer function plots from DTT XML-based output.

Usage:

  * `dtt2tfplot [-v] [-o output_dir] ch1.xml ... chN.xml`
